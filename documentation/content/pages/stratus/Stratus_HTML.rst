.. -*- mode: rst; explicit-buffer-name: "Stratus_HTML.rst<pelican>" -*-


==================================
Stratus : Netlist Capture Language
==================================


:slug:    stratus-language
:date:    2020-01-02 16:00
:Authors: Sophie Belloeil
:Contact: <Jean-Paul.Chaput@lip6.fr>
:Version: June 4, 2019 (jpc)
:status:  hidden


.. include:: ../../../etc/definitions.rst

.. |add1|     image:: {attach}images/add1.png
.. |add2|     image:: {attach}images/add2.png
.. |addaccu|  image:: {attach}images/addaccu.png
.. |test|     image:: {attach}images/test.png
.. |editor|   image:: {attach}images/editor.png
.. |resize|   image:: {attach}images/resizeAb.png
.. |xml|      image:: {attach}images/xml.png


Printable version of this document `Stratus.pdf <{filename}/pdfs/Stratus.pdf>`_.

Stratus – Procedural design language based upon *Python*

.. contents::
   :depth: 1

.. include:: Language.rst
